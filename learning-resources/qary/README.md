# qary

## Table of Contents
1. [What is qary?](#1-intro)
2. [How to use qary](#2-how-to-use-qary)
3. [Getting started](#3-getting-started)

## 1. Introduction

Meet qary, a conversational assistants that won't manipulate you or betray your privacy.
Think of it like a mini-Siri, if you could run it on your laptop and didn't have to share your questions with Apple or anyone else.

## 2. How to use qary

The [README.md](https://gitlab.com/tangibleai/qary/-/blob/master/README.md)file on the repository will show you how to install and run qary on your laptop. 
It's a command line tool.

## 3. Getting Started

The docs are a great place to start.

- [Your First Qary Skill](https://gitlab.com/tangibleai/qary/-/blob/master/docs/Your%20First%20Qary%20Skill.md)
- [Qary docs](docs.qary.ai)

