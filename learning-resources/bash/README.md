### Introduction
- What does Bash stand for?
	- Bourne again shell (Bash).
- What is a shell?
	- The software that interprets and executes the commands that are typed into the terminal. The Bash shell is a particularly popular shell that is found on Unix-like systems.
	- What is a terminal?
		- The command-line user interface window that you see on the screen.
- What does CLI stand for?
	- Command Line Interface (CLI)

---
### Root Directories -- in the CLI
- What does the /bin directory have in it?
	- It stores certain commonly used programs known as binary files.
- What does the /usr/bin directory have in it?
	- It also stores commonly used programs. It is another location for commonly used programs.
- What does the /etc directory have in it?
	- It stores config files for the system.
- What does the /var/log directory have in it?
	- It stores system log files -- for various system programs.

---
### Paths -- in the CLI
- What is the . (dot or full stop) a reference to?
	- The current directory.
- What is .. (dotdot or two full stops) a reference to?
	- The parent directory.
	- How can ".." be used to refer to directories?
		- ``../../home/user``
		- What type of path is the above example?
			- A relative path.
			- What is the other type of path called?
				- Absolute path
				- What is an example of an absolute path?
					- ``/home/user/src``
						- The '/' (forward slash) character denotes the root directory.
- How can spaces be used in paths without the command line throwing an error?
	- Using quotes or the escape character (\)
- If you use TAB Completion before encountering the space in a path name, what will the CLI do for you?
	- It will add escape characters before the spaces in the path name automatically.

---
### Commands
- How can you invoke several options in a command?
	- You can put a single dash with each single letter representing the option desired together after the dash.
		- This is the case if the option does not have an argument that goes with it. If the option does have an argument with it, than the option must be by itself.
- What is the command to make a directory?
	- ``mkdir [options] <Directory>``
		- This is short for "make directory".
- What is the command to remove an empty directory?
	- ``rmdir [options] <Directory>``
		- This is short for "remove directory". This command only works with empty directories.
		- Be careful, there is no "undo" on the command line after removing a directory.
- What is the command that allows you to change the file timestamp?
	- ``touch [options] <File>``
		- What is a 'side effect' of trying to change a file with the touch command if the file does not exist?
			- The file will be created.
- What command is used to copy a file or directory?
	- ``cp <Source_path> <Destination_path>``

---
### Files
- What is the command in Linux that can tell the user what file type a file is?
	- ``file <File>``
- Everything in Linux is a file, everything.
- Files are case sensitive.

---
### Manual
- What is the command to view the manual for a system command?
    - ``man <command>``
- How can you search all the manuals for system commands?
    -``man -k <search_term>``
- How can you search within a system command manual reference?
	- When you are inside the manual reference page for the system command, use /, then type whatever term you want to search, and then press enter.
	-``/<term>``
	- What key can be pressed to cycle through the search terms found if there are more than one?
		- The key 'n' can be pressed to cycle through the searched terms in the reference page.

---
### See also
- Linux
- Unix-like operating systems
- zsh
- fish
- sh

---
### Reference
- https://ryanstutorials.net/linuxtutorial/
