# Final Reports

- [Quote Recommender](billy-quote-recommender.md) by Billy Horn (Winter 2021)
- [Word Map](jon-wordmap.ipynb) by Jon Sundin (Winter 2021)
- [Vaccination Uptake](winston-vaccination-report.docx)
- TBD: [Rap Recommender]() by Uzi Ikwakor (Winter 2021)
- TBD: [Neural Machine Translation] by Hanna Seyoum (Winter 2021)

