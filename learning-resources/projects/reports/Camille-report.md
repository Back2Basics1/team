# Spring 2021 Intern Project by Camille Dunning
## Problem
Given a set of linear dialogs, create a single **graph** structure with forks when the dialog intent changes, and a multi-degree node when two dialogs reach the same state. We aim to effectively *merge two dialogs together*, and create an **intent-based dialog graph**.

This is a **software development** project involving contributions to the dialog turning functionality of the `qary` library. Here, I present an MVP for the proposed algorithm. 

## Use Case and Problem Flow
An example use case for this would be handling recorded conversations between a teacher bot and student the student's ideas for their personal statements for college applications. Suppose that in one instance, a student knows what he wants to write about, and in another, a student doesn't. 

In both of these dialogs, the teachers start by asking the student to confirm that this will be their assignment. Hence, the teachers are in the same **state**, which `qary` may encode as `introduction`. This single state will be represented in the node of the resulting graph. The next ingregient is a **next condition**, which is expressed as a set of all possible categories for responses. A likely next condition will be `assignment confirmed` if both students confirm the assignment. The `assignment confirmed` condition yields a set of possible responses, including "y", "you got it", or any variation of "yes".

If this next condition is fulfilled, it becomes the conversation's new **state**. The teacher bot then asks a new question based on the current state (`assignment confirmed`), for example, "Do you know what you want to write about?" 

Suppose that both of the students answer differently to this question. Thus, the next conditions for `assignment confirmed` will be different. This will create a **fork** in the graph (i.e., two new nodes are created instead of one), accentuating the *differences between intents*.

**Currently, this algorithm only handles cases where humans respond the same, but there might be variations in how the bot directs the conversation.**

## Implementation
![Pipeline](Tangible_AI_Pipeline.png)
### Step 1: Input
This algorithm takes in a list of dictionaries, as defined in qary's `script_to_dialog()` function.
```
[
    {
        'state': 'turn_id_0',
        'bot': '<Introduction or greeting>' <-- BOT STATEMENT
        'next_condition': {'turn_id_1': <Human greeting>} <-- WHAT HUMAN RESPONDS
    },
    {
        'state': 'turn_id_1',
        'bot': '<Question>'
        'next_condition': {'turn_id_2': <Human answer>}
    },
    {
        'state': 'turn_id_2',
        'bot': '<Statement>'
        'next_condition': {'turn_id_3': <Human response>}
    }
]
```
### Step 2: Dialog Preprocessing
```python
dlg_1 = list(filter(lambda x: not x['state'].endswith('__'), dlg_1))
...
t['state'] = f'd{i + 1}_' + t['state']
t['next_condition'] = dict([
    (f'd{i + 1}_' + statename, statement) for (statename, statement) in t.get('next_condition', {}).items()
])
```
This code prepends `d1_`, for instance, to each of dialog 1's state names and `next_condition` keys. This also removes the meta-key-value pairs.
### Step 3: Initial Unmerged Mapping
```python
merge_mapping[dlg_1[i]['state']] = [dlg_1[j + 1]['state']]
```
Turn the list-of-dict mapping into a dictionary one by assigning as values each state's next dialog state in the sequence.
### Step 4: Adding More Statements to Mapping
```python
for turn in dlg_1:
    if turn['state'] == v[0]:
        bot_statement = turn['bot']
            for other_turn in dlg_2:
                if is_similar(bot_statement, other_turn['bot']):
                    v.append(other_turn['state'])
```
## Results
Example of a two dialogs with a college essay assistance bot, with `turn_id_0` being greetings/assignment confirmations and `turn_id_1` asking if the student knows what to right.
<img src="Screen_Shot_2021-06-25_at_04.14.56.png" width="240"/>
<img src="Screen_Shot_2021-06-25_at_04.15.46.png" width="400"/>

In this particular case, `d1_turn_id_0` and `d2_turn_id_0` were the same, so they both had connections to `d1_turn_id_1` and `d1_turn_id_2`, which were also the same. However, the algorithm did not recognize these two strings as similar, although they had the same intent:
1. Great! Were you on the soccer team this year?
2. Excellent! Did you participate in this year's FIRST robotics competition?

This indicates that we need a much more reliable measurement of similarity. These instances did not have the same connections in the graph as the ones shown above. Having a different measure of similarity will be the main improvement for this MVP.

## Other Algorithm improvements
1. Even more reliable than pure string similarity, we can change the input format to include an indicator of intent. This would help store two of the same statements in one node, rather than two separate ones. 
![Two Nodes](nodes.png)
2. Input more than two dialogs, but this is *very* computationally expensive. The current algorithm is already *O*(*m* * *n*) + *O*(*m* + *n*), where 
*m* and *n* are the lengths of the first and second dialogs, respsectively.
3. When creating the initial mapping, take into account the `turn_id` indicated in the `next_condition` key-value pair, rather than just mapping to the next dictionary in the list.
