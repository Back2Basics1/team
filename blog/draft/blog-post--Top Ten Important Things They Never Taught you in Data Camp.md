# Top Ten Things They Never Taught you in Data Camp

1. NaNs are a good thing (use .isna() to double your feature count)
2. Residual scatter plots are the only visualizaiton worth
3. The plotting residual error (y-axis) relative vs the true target value (x-axis) does **not** provide actionable insight
4. Don't waste your time trying to normalize a skewed distribution of your target variable with Scaling, Undersampling, or Oversampling
5. A low accuracy model can often be worth more money to your business than a high accuracy one (F1 score, more regularized (future-proof), more explainable models are worth more)
6. Machines are better at EDA than humans
7. EDA is mainly useful for data cleaning
8. Colinear variables should nver be pruned during feature selection
9. Never do feature selection unless you're sure you have an overfitting problem that you can't deal with in another way.
10. You are better at tuning a model than a machine (never use GridSearch)
11. Calculating P-Value is a waste of time
12. Always balance your target classes
13. (never use undersampling)
14. (never use oversampling)
15. Never scale your data, scalers are only useful at the end, in interpretting your coefficients
16. 1-Fold CrossValidation is better than K-Fold
17. You must think hard and deep about every model you train if you want to beat the Robot Overlords
18. Data Science (AI) will destroy society in 20 years, unless **you** do things differently than my generation did
19. If you're using Spark or HPC in the first year of a DS project, you're doing it wrong.
20. A top 10 list is never enough, whether it's click bait search results or blog titles
21. Domain knowledge hinders your ability to see what the cool things the data (and your model) are telling you
22. Regularly check what you learn from school or online with an actual data science experiment
23. Synthetic data is often a better source of insights about a problem or a model, than than real world data or models trained on it
24. Train the simplest most explainable model (to you) first
25. Boosted trees are magic... that can often distract you from better models.
26. Don't bother filtering arbitrary sets of stopwords or punctuation marks until the data proves that it helps.
27. Stemming and lemmatization should only every be considered if you have a full-text search recall issue.
28. Use BPE or WordPiece tokenization instead of conventional word tokenization, unless you have a good reason not to.
29. GloVE is better than Word2Vec.
30. BERT and USE embeddings are best for phrases longer than 2 words and shorter than a paragraph.

Don't get me started...

# 11 Data Science Techniques You'll' Unlearn in Your First Week at Tangible AI

1. df.dropna(), df.fillna('interp'), df.fillna('mean')
2. seaborn.scatterplot(x=df['true_price'], y=df['predicted_price'] - df['true_target'])
3. seaborn.scatterplot(x=df['true_price'], y=df['predicted_price'] - df['true_target'])
4. OLS works best on normally distributed data
5. accuracy is all that matters
6. You need to do EDA before you can get insight into a problem
16. GridSearchCV, GridSearch, and CrossValidation are invaluable
23. XGBoost will solve all your problems
